﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public bool hasBrick;
    private PlayerMovement playerMovement;
    public float vision;
    public float health;
    public float armor;
    public float maxHealth;
    public bool teamRed;
    public bool hasFood;


    private void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();

        Material mat = GetComponent<Renderer>().material;

        if (teamRed)
        {
            mat.color = Color.red;
        }
        else
        {
            mat.color = Color.blue;
        }

    }

    public bool PickUpBrick()
    {
        if (hasBrick)
        {
            return false;
        }
        else
        {
            hasBrick = true;
            return true;
        }
    }

    public void ConsumeItem(Item item, GameObject itemObj)
    {
        Item copy = item;
        Destroy(itemObj);

        if (item.costsFood)
        {
            hasFood = false;
        }
        else
        {
            hasFood = true;
        }

        playerMovement.agent.speed += copy.speed;
        vision += copy.vision;
        health += copy.health;
        armor += copy.armor;
        maxHealth += copy.maxHealth;
        StartCoroutine(StopItemEffects(copy, itemObj));
    }

    IEnumerator StopItemEffects(Item i, GameObject itemObj)
    {
        yield return new WaitForSeconds(i.activationTime);

        playerMovement.agent.speed -= i.speed;
        vision -= i.vision;
        armor -= i.armor;
        maxHealth -= i.maxHealth;

        
    }

}
