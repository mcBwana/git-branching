﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour {

    public float radius = 3f;
    public float interactionTime;
    

    private bool isFocus = false;
    private Transform player;
    public Transform interactionTransform;

    public void Interrupt()
    {
        StopAllCoroutines();
    }

    private void Update()
    {
        if (isFocus)
        {
            if (Vector3.Distance(player.position, interactionTransform.position) <= radius)
            {
                player.GetComponent<PlayerMovement>().StopAgent();
                StartCoroutine(Interact());
            }
            else
            {
                Interrupt();
            }
        }
        
    }

    public virtual void Interaction(Player player)
    {
        Debug.Log("Interaction with: " + interactionTransform.name);
    }
    
    IEnumerator Interact()
    {
        Debug.Log("xD");
        yield return new WaitForSeconds(interactionTime);

        if(player != null && isFocus)
        {
            Debug.Log("Interaction");
            Interaction(player.GetComponent<Player>());
        }
    }

    public void Focused(Transform player)
    {
        isFocus = true;
        this.player = player;
    }

    public void DeFocused()
    {
        isFocus = false;
        player = null;
    }

    private void OnDrawGizmosSelected()
    {
        if(interactionTransform != null)
        {
            interactionTransform = transform;
        }
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }
}
