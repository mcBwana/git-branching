﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody rb;
    public NavMeshAgent agent;
    public Camera cam;
    public Interactable focus;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        agent = GetComponent<NavMeshAgent>();

    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit))
            {
                agent.SetDestination(hit.point);
                agent.stoppingDistance = 0;
                if(focus != null)
                focus.Interrupt();
                RemoveFocus();
            }

        }

        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                Interactable inter = hit.transform.GetComponent<Interactable>();

                if (inter != null)
                {
                    FocusItem(inter);
                    agent.SetDestination(inter.interactionTransform.position);
                    agent.stoppingDistance = inter.radius;
                }
            }
        }

        

    }

    public void StopAgent()
    {
        agent.SetDestination(transform.position);
    }

    public void FocusItem(Interactable inter)
    {
        if(inter != focus)
        {
            if(focus != null)
            {
                focus.DeFocused();
            }

            focus = inter;

        }

        focus = inter;
        inter.Focused(transform);
    }

    public void RemoveFocus()
    {
        if(focus == null)
        {
            return;
        }

        focus.DeFocused();
        focus = null;
        
    }

}
