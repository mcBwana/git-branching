﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Homebase : MonoBehaviour {

    public float radius;
    public bool teamRed;
    public int bricks;
    public Player[] players;
    public GameObject brickPrefab;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, radius);
    }
    
    private void Update()
    {
        players = FindObjectsOfType<Player>();

        for (int i = 0; i < players.Length; i++)
        {
            if(Vector3.Distance(players[i].transform.position, transform.position) < radius && players[i].teamRed == teamRed)
            {
                if (players[i].hasBrick)
                {
                    players[i].hasBrick = false;
                    bricks += 1;
                    SpawnBrick();
                }
            }
        }

    }

    void SpawnBrick()
    {

        Vector3 pos = new Vector3(Random.Range(-radius, radius) + transform.position.x, transform.position.y + 4f, Random.Range(-radius, radius) + transform.position.z);
        Material mat = Instantiate(brickPrefab, pos, Quaternion.identity).GetComponent<Renderer>().material;
        Color col = new Color();
        if (teamRed)
        {
            col = Color.red;
        }
        else
        {
            col = Color.blue;
        }

        mat.color = col;
    }

}
