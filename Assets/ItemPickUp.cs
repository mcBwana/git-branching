﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickUp : Interactable {

    public Item item;

    private void Awake()
    {
        SphereCollider sphereCol = GetComponent<SphereCollider>();
        if (sphereCol == true)
        {
            sphereCol.radius = radius - 0.2f;
        }

        if (GetComponent<Renderer>() == null)
        {
            return;
        }
        Material mat = GetComponent<Renderer>().material;

        if (item.teamRed)
        {
            mat.color = Color.red;
        }
        else
        {
            mat.color = Color.blue;
        }
    }

    public override void Interaction(Player player)
    {
        base.Interaction(player);

        PickUp(player);
    }

    void PickUp(Player player)
    {
        Debug.Log(player);
        if (item.brick)
        {
            if(player.teamRed == item.teamRed)
            {
                bool gotPickedUp = player.PickUpBrick();
                Destroy(gameObject);

                if (!gotPickedUp)
                {
                    Debug.Log(player + " Has a brick already");
                }
            }
            else
            {
                Debug.Log("Wrong team! Cant pickup");
            }
            

        }
        else
        {
            if (player.hasFood)
            {
                player.ConsumeItem(item, gameObject);
            }
            else
            {
                Debug.Log("No food!");
            }

        }
    }

}
