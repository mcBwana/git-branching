﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Items/Item")]
public class Item : ScriptableObject {

    new public string name;
    public float speed;
    public float vision;
    public float health;
    public float activationTime;
    public float armor;
    public float maxHealth;
    public bool teamRed;
    public bool brick;
    public bool costsFood;
    

}
