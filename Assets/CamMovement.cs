﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMovement : MonoBehaviour
{

    public Transform target;
    public Vector3 offset;
    public float camSpeed;
    public float followRange;
    public bool centerMode;

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(target.position, followRange);
    }

    private void LateUpdate()
    {
        Vector3 wantedPos = target.position + offset;

        Vector3 diff = wantedPos - transform.position;

        if (centerMode)
        {
            transform.position = wantedPos;
        }

        

    }

}
